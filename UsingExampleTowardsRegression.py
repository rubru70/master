#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 21:04:02 2020

@author: brugmanr
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pylab

#https://towardsdatascience.com/introduction-to-linear-regression-in-python-c12a072bedf0
#read dataset
df = pd.read_excel('/home/brugmanr/Documents/Rudolf/uitproberen/voorbeeld15.2.xlsx')  

#convert year to date
df['date'] = pd.to_datetime(df['Jaar'], format="%Y")
#del df1['Jaar']

df.rename(columns={"Jaar":"X", "Waarneming":"y"}, inplace=True)
#df1.columns = ["var1","var2","var3","var4"]

# Calculate the mean of X and y
xmean = np.mean(df["X"])
ymean = np.mean(df["y"])

# Calculate the terms needed for the numator and denominator of beta
df['xycov'] = (df['X'] - xmean) * (df['y'] - ymean)
df['xvar'] = (df['X'] - xmean)**2

# Calculate beta and alpha
beta = df['xycov'].sum() / df['xvar'].sum()
alpha = ymean - (beta * xmean)
print(f'alpha = {alpha}')
print(f'beta = {beta}')

ypred = alpha + beta * df["X"]

# Plot regression against actual data
plt.figure(figsize=(12, 6))
plt.plot(df["X"], ypred, 'b--', linewidth=1)     # regression line
#plt.plot(df["X"], df["y"], 'ro-')   # scatter plot showing actual data
plt.plot(df["X"], df["y"], color='green', marker='o',
         markerfacecolor='red', linestyle='dashed', linewidth=2, markersize=12)
plt.title('Actual vs Predicted')
plt.xlabel('X')
plt.ylabel('y')
#plt.acorr(df["X"])
plt.show()

##AUTOMATIC
import statsmodels.formula.api as smf
# Initialise and fit linear regression model using `statsmodels`
model = smf.ols("y ~ X", data=df)
model = model.fit()

model.params
ypred2 = model.predict()# Predict values

plt.figure(figsize=(12, 6))
plt.plot(df["X"], ypred2, 'b--', linewidth=1)     # regression line
plt.plot(df["X"], df["y"], color='green', marker='o',
         markerfacecolor='red', linestyle='dashed', linewidth=2, markersize=6)
plt.title('Actual vs Predicted')
plt.xlabel('X')
plt.ylabel('y')
plt.show()

#AUTOMATIC2
from sklearn.linear_model import LinearRegression

# Build linear regression model using X as predictor
# Split data into predictors X and output Y
#predictors = ['X', 'y']
predictors = ['X']
X = df[predictors]
y = df['y']

# Initialise and fit model
lm = LinearRegression()
model = lm.fit(X, y)

print(f'alpha = {model.intercept_}')
print(f'betas = {model.coef_}')

ypred3 = model.predict(X)

plt.figure(figsize=(12, 6))
plt.plot(df["X"], ypred3, '--', color='orange', linewidth=1)     # regression line
plt.plot(df["X"], df["y"], color='green', marker='o',
         markerfacecolor='red', linestyle='dashed', linewidth=2, markersize=6)
plt.title('Actual vs Predicted')
plt.xlabel('X')
plt.ylabel('y')
plt.show()
