#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 17:07:53 2020

@author: brugmanr
"""
from statsmodels.tsa.ar_model import AR
from random import random

data = [x + random() for x in range(1,100)]

model = AR(data)
model_fit = model.fit()

yhat = model_fit.predict(len(data), len(data))
yhat = model_fit.predict(99,101)

print(yhat)
