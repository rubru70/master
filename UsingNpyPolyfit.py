#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 21:04:02 2020

@author: brugmanr
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy
import pylab

#read dataset
df1 = pd.read_excel('/home/brugmanr/Documents/Rudolf/uitproberen/voorbeeld15.2.xlsx')  

#convert year to date
df1['date'] = pd.to_datetime(df1['Jaar'], format="%Y")
del df1['Jaar']

#create timeserie object
ts1 = df1.set_index('date')

#plot
plt.plot(ts1)
plt.grid(True)
plt.show()

#add trend line
x = ts1
y = ts1["Waarneming"]
#pylab.plot(df1["date"],df1["Waarneming"],"o")
pylab.plot(x,y,"o")

# calc the trendline (it is simply a linear fitting)
z = numpy.polyfit(x, y, 1)
p = numpy.poly1d(z)
plt.plot(x,p(x),"r--")
# the line equation:
plt.title("y=%.6fx+%.6f"%(z[0],z[1])) 
print("y=%.6fx+(%.6f)"%(z[0],z[1]))

df2 = pd.melt(df1)

df3 = df2[df2.variable != "kwartaal"].reset_index(drop=True)

l_kwartaal2 = []
for i in range(len(df3)): 
  #l_kwartaal2.insert(i, df3.loc[i, "variable"]+"-"+str(i%4+1))
  l_kwartaal2.insert(i, str(df3.loc[i, "variable"])+"-Q"+str(i%4+1))
df3['kwartaal2'] = l_kwartaal2

df3['date'] = pd.to_datetime(df3['kwartaal2'])
df3 = df3.set_index('date')
del df3['kwartaal2']
del df3['variable']

import matplotlib.pyplot as plt
plt.plot(df3,'b')
plt.grid(True)

import matplotlib.pyplot as plt

plt.ylabel('some numbers')
plt.plot([1, 2, 3, 4])
plt.show()

plt.plot([1, 2, 3, 4], [1, 4, 9, 16])
plt.ylabel('some numbers')

plt.plot([1, 2, 3, 4], [1, 4, 9, 16], 'ro')
plt.axis([0, 6, 0, 20])
plt.show()

import numpy as np

# evenly sampled time at 200ms intervals
t = np.arange(0.0, 5.0, 0.2)
# red dashes, blue squares and green triangles
plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
plt.show()

#plotting with keywords
data = {'a': np.arange(50),
        'c': np.random.randint(0, 50, 50),
        'd': np.random.randn(50)}
data['b'] = data['a'] + 10 * np.random.randn(50)
data['d'] = np.abs(data['d']) * 100

plt.scatter('a', 'b', c='c', s='d', data=data)
plt.xlabel('entry a')
plt.ylabel('entry b')
plt.show()

# data for histogram
mu, sigma = 100, 15
x = mu + sigma * np.random.randn(10000)
# the histogram of the data
n, bins, patches = plt.hist(x, 50, density=1, facecolor='g', alpha=0.75)


plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
plt.axis([40, 160, 0, 0.03])
plt.grid(True)
plt.show()

#annotate text
ax = plt.subplot(111)

t = np.arange(0.0, 5.0, 0.01)
s = np.cos(2*np.pi*t)
line, = plt.plot(t, s, lw=2)

plt.annotate('local max', xy=(2, 1), xytext=(3, 1.5),
             arrowprops=dict(facecolor='black', shrink=0.05),
             )

plt.ylim(-2, 2)
plt.show()
